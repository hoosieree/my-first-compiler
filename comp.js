// tokenize -> parse -> codegen [ -> eval -> run ]
'use strict';
function tokenize(s){
  const token_classes=[
    ['def', /\bdef\b/],
    ['end', /\bend\b/],
    ['identifier', /\b[a-zA-Z]+\b/],
    ['integer', /\b[0-9]+\b/],
    ['oparen', /\(/],
    ['cparen', /\)/],
    ['comma', /,/]
  ];
  // traverse token classes in-order, so 'def' and 'end' match as keywords (not identifiers)
  function tk1(str){
    for(let tk of token_classes){const rx=tk[1].exec(str); if(rx && rx.index === 0){return({type:tk[0], value:rx[0]});}}
    throw`no token matched: ${str}`;
  }
  // peel tokens off the front of the string until the string is empty
  let tokens=[];
  while((s = s.trim()).length){const t1=tk1(s); if(t1){tokens.push(t1); s = s.slice(t1.value.length);}}
  return tokens;
}

function parse(tokens){
  // utils
  function peek(expected, offset=0){return tokens[offset].type === expected;}
  function eat(expected){
    const t=tokens.shift();
    if(t.type === expected){return t;}
    throw`expected ${expected} but got ${t.type}`;
  }

  // terminal nodes
  function parse_arg_names(){
    const arg_names=[];
    eat('oparen');
    if(peek('identifier')){arg_names.push(eat('identifier').value); while(peek('comma')){eat('comma'); arg_names.push(eat('identifier').value);}}
    eat('cparen');
    return arg_names;
  }
  function parse_integer(){return IntegerNode(parseInt(eat('integer').value, 10));}
  function parse_var_ref(){return VarRefNode(eat('identifier').value);}

  // branching (recursive) nodes
  function parse_call(){const name=eat('identifier').value, arg_exprs=parse_arg_exprs(); return CallNode(name, arg_exprs);}
  function parse_arg_exprs(){
    const arg_exprs=[];
    eat('oparen');
    if(!peek('cparen')){arg_exprs.push(parse_expr()); while(peek('comma')){eat('comma'); arg_exprs.push(parse_expr());}}
    eat('cparen');
    return arg_exprs;
  }
  function parse_expr(){
    if(peek('integer')){return parse_integer();}
    if(peek('identifier') && peek('oparen', 1)){return parse_call();}
    return parse_var_ref();
  }
  function parse_def(){
    eat('def');
    const name=eat('identifier').value, arg_names=parse_arg_names(), body=parse_expr();
    eat('end');
    return DefNode(name, arg_names, body);
  }

  // node classes
  function DefNode(name, arg_names, body){return({class:'DefNode', name, arg_names, body});}
  function IntegerNode(value){return ({class:'IntegerNode', value});}
  function CallNode(name, arg_exprs){return ({class:'CallNode', name, arg_exprs});}
  function VarRefNode(value){return ({class:'VarRefNode', value});}

  return parse_def(); // entry point
}


function codegen(node){
  switch(node.class){
    // terminal nodes
  case'IntegerNode':return`${node.value}`;break;
  case'VarRefNode':return`${node.value}`;break;
    // branching (recursive) nodes
  case'DefNode':return`function ${node.name}(${node.arg_names.join(',')}){return ${codegen(node.body)};}`;break;
  case'CallNode':return`${node.name}(${node.arg_exprs.map(codegen)})`;break;
    // everything else
  default:throw`unexpected node type: ${JSON.stringify(node)}`;break;
  }
};


// test
const SRC=`def f(x,y) add(y,x) end`,
      RUNTIME='function add(x,y){return x+y;}',
      TEST='f(1,2)',
      tokenized=tokenize(SRC),
      parsed=parse(tokenized),
      generated=codegen(parsed),
      STR=[RUNTIME, generated, TEST].join('\n');
console.log(eval(STR)); // should print 3
